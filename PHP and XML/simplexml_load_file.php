<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Homepage</title>
	<link rel="stylesheet" href="source/css/style.css">
	<link rel="stylesheet" href="source/bootstrap/bootstrap-3.3.6/css/bootstrap.min.css">
	<link rel=stylesheet href="source/bootstrap/bootstrap-3.3.6/css/bootstrap-theme.css">
	<script src="source/bootstrap/bootstrap-3.3.6/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="source/jquery/jquery-2.2.1.js" type="text/javascript" charset="utf-8"></script>
	<script src="source/jquery.mobile/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="source/jquery.mobile/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">
	<script src="source/jquery.ui/jquery-ui-1.11.4/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="source/jquery.ui/jquery-ui-1.11.4/jquery-ui.min.css">
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">

			</div>
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
				<?php 
					$fileXML = simplexml_load_file("content.xml");
					echo $fileXML->content[0]['category'] . "<br>";
					echo $fileXML->content[0]->from['id'];

					echo $fileXML->content[0]->from . "<br>";
					echo $fileXML->content[0]->paragraph;
				 ?>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
				<script src="source/javascripts/application.js" type="text/javascript" charset="utf-8"></script>
			</div>
		</div>
	</div>
</body>
</html>